import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base'
// import './users'
import { Reservations,Nugget ,MyNotification,TypesNuggets,MyComments} from './Collections/index'
import { google } from 'googleapis';
import key from "./DriveApi-9184b9e4ec82.json";
import fs from 'fs'
import base64img from 'base64-img';

Meteor.startup(() => {



  

  if (Meteor.isClient) {
    Accounts.onCreateUser(function (options, user) {
      user.Country = "";
      user.UserType = "";
      user.UserName = ""
      return user;
    })
  }
  if (Meteor.isServer) {
    Accounts.onCreateUser(function (options, user) {
      user.profile = options.profile || {};

      user.profile.Country = options.Country;
      user.profile.UserType = options.UserType;
      user.profile.UserName = options.UserName

      return user;
    })
  };
  Reservations.allow
    ({
      insert: function (userId, doc) {
        return true;
      }
      ,
      update: function (userId, doc, fieldNames, modifier) {
        return true;
      }
      ,
      remove: function (userId, doc) {
        return true;
      }

    })
  Meteor.users.allow({
    insert: function (userId, doc) {
      return true;
    }
    ,
    update: function (userId, doc, fieldNames, modifier) {
      return true;
    }
    ,
    remove: function (userId, doc) {
      return true;
    }
  });

// #region  dbCollection publication 
  Meteor.publish('nuggets', () => {

    return Nugget.find({})
  })

  Meteor.publish('myComments', () => {

    return MyComments.find({})
  })

  Meteor.publish('Types', () => {

    return TypesNuggets.find({})
  })
  Meteor.publish('reservations', () => {

    return Reservations.find({})
  })
  Meteor.publish("myuser",
    function () {
      return Meteor.users.find({ _id: this.userId })
    }
  );

  Meteor.publish('Mynotification', () => {
    return MyNotification.find({TouserID: Meteor.userId()})
  })
// #endregion  

  Meteor.methods({
    'addComment'(comment) {
      if (!this.userId) {
        throw new Meteor.Error('not-authorized');
      }
      else {

        MyNotification.insert({
          RseservationId: Notify.RseservationId,
          FromuserID: Notify.FromuserID,
          FromuserName: Notify.FromuserName,
          TouserID: Notify.TouserID,
          content: Notify.content,
          status: Notify.status,
          NotificationType: Notify.NotificationType

        })
      }
    },

    'addNotification'(Notify) {
      if (!this.userId) {
        throw new Meteor.Error('not-authorized');
      }
      else {

        MyNotification.insert({
          RseservationId: Notify.RseservationId,
          FromuserID: Notify.FromuserID,
          FromuserName: Notify.FromuserName,
          TouserID: Notify.TouserID,
          content: Notify.content,
          status: Notify.status,
          NotificationType: Notify.NotificationType

        })
      }
    }
    ,
    'getallfiles'() {
let koko=[]
     return new Promise((resolve,reject) => {
        let jwtToken = new google.auth.JWT(
          key.client_email,
          null,
          key.private_key,
          ["https://www.googleapis.com/auth/drive"],
          null
        );
        let drive = google.drive('v3');
        let filesBase64 = [];
        var parents = "1WqR5Q5rQ1PeSI4dCHMLSV8-CEflCihnb"
        drive.files.list({

          auth: jwtToken,
          pageSize: 10,
          q: "'" + parents + "' in parents and trashed=false",
          fields: 'files(id, name)',
        }, (err, {
          data
        }) => {
          if (err) return console.log('The API returned an error: ' + err);
          const files = data.files;
          if (files.length) {
            files.map((file) => {
              console.log('Files:');
              console.log(`${file.name} (${file.id})`);
             
              drive.files.get({
                auth: jwtToken,
                fileId: file.id,
                alt: 'media'
                },{
                    responseType: 'arraybuffer',
                    encoding: null
                }).then((res) => {
                var imageType = res.headers['content-type'];
                var base64 = new Buffer(res.data, 'utf8').toString('base64');
                var dataURI = 'data:' + imageType + ';base64,' + base64;
                filesBase64.push(dataURI);
                if(filesBase64.length === files.length)
                {
                  console.log('file base64 : ',filesBase64)
                  resolve(filesBase64);
                }
              })
                
          } );
          //console.log('filesBase64 : ' , filesBase64);
        
        }
          else {
            console.log('No files found.');
          }
        });

       })
    },
    'cancellReservationDeleteNotification'(notificationobj) {
      Reservations.remove({ reservationId: notificationobj.RseservationId })
      MyNotification.remove({ notificationId: notificationobj.notificationId })
    },

    'AcceptReservationDeleteNotification'(notificationobj) {
      Reservations.update({ reservationId: notificationobj.RseservationId }, { $set: { status: "Accepted" } });
      MyNotification.remove({ notificationId: notificationobj.notificationId })
    },

    "Converttoonline"(Image) {

      return new Promise((resolve, reject) => {



        let drive = google.drive('v3');
        let jwtToken = new google.auth.JWT(
          key.client_email,
          null,
          key.private_key,
          ["https://www.googleapis.com/auth/drive"],
          null
        );
        jwtToken.authorize((error) => {
          if (error) {
            console.log('error ', error);
          }
          else {
            console.log('Auth Succeeded');

          }
          base64img.img(Image.uri, 'E:/images', 'koko', function (err, filepath) {
            console.log('error ', err);
            console.log('filepath ', filepath);
            var folderId = "1WqR5Q5rQ1PeSI4dCHMLSV8-CEflCihnb";
            var fileMetadata = {
              'name': 'koko.jpg',
              parents: [folderId]
            };
            var media = {
              mimeType: 'image/jpeg',
              body: fs.createReadStream('E:/images/koko.jpg')
            };
            drive.files.create({
              auth: jwtToken,
              resource: fileMetadata,
              media: media,
              fields: 'id'
            }, function (err, file) {
              if (err) {

                console.error('error while uploading', err);
              } else {

                drive.files.get({
                  auth: jwtToken,
                  fileId: file.data.id,
                  alt: 'media'
                }, {
                    responseType: 'arraybuffer',
                    encoding: null
                  }).then((res) => {
                    console.log(res);
                    console.log('done');
                    var imageType = res.headers['content-type'];
                    var base64 = new Buffer(res.data, 'utf8').toString('base64');
                    var dataURI = 'data:' + imageType + ';base64,' + base64;
                    // _.img(dataURI, 'D:/', '123', function(err, filepath) {
                    //     console.log(filepath)
                    // });
                    resolve(dataURI);


                  })


              }
            });
          });





        });

      })
    },


    'addReservation'(NewDate) {

      console.log("NewDate", NewDate)
      if (!this.userId) {
        throw new Meteor.Error('not-authorized');
      }
      else {
        debugger;
        Reservations.insert({

          reservationId: NewDate.reservationId,
          RecieverID: this.userId, Event: {
            start: NewDate.Event.start,
            end: NewDate.Event.end,
            Notes: NewDate.Event.Notes,
            title: NewDate.Event.title
          }
          , status: NewDate.status,
          ProviderID: NewDate.ProviderID
        })
      }
    }
    ,
    'addNuggets'(NewNuggets) {
      console.log(NewNuggets)
      if (!this.userId) {
        throw new Meteor.Error('not-authorized');
      }
      else {
        debugger;
        Nugget.insert({
          NuggetID:NewNuggets.NuggetID,
          userID: this.userId, ProviderName: NewNuggets.ProviderName,
          URL: NewNuggets.URL, Text: NewNuggets.text,
          PositionImage: { x: NewNuggets.postionImage.x, y: NewNuggets.postionImage.y, offsetx: NewNuggets.postionImage.offsetX, offsety: NewNuggets.postionImage.offsetY },
          PositionText: { x: NewNuggets.postionText.x, y: NewNuggets.postionText.y, offsetx: NewNuggets.postionText.offsetX, offsety: NewNuggets.postionText.offsetY }
          ,

        })
        TypesNuggets.insert({
          NuggetID:NewNuggets.NuggetID,
          Type: NewNuggets.Type,
          AvailableTimes:NewNuggets.AvailableTimes,
          From:NewNuggets.From,
          To:NewNuggets.To

        })

      }
    },

  }


  )

})

  //   'addToAdmin'(NewDate) {
  //     let Admin = Meteor.users.findOne({ isAdmin: true })
  //     console.log(Admin._id)
  //     Meteor.users.update({ _id: Admin._id }, { $addToSet: { dates: NewDate } })

  //   },


  //     Meteor.users.update({ _id: Meteor.userId() }, { $pull: { "dates": { "ID": date.ID } } })
  //     console.log(Meteor.user())

  //   }


  //       { CustomerName: 'koko',
  //   ZoneNumber: '2',
  //  FromTime: 2018-06-13T15:23:11.699Z,
  //   ToTime: 2018-06-13T15:56:20.440Z,
  //   currentData: '2018-06-13T00:47:56+02:00' }

  //   */

  //   'CheckAvailablity'(values) {
  //     //console.log("users",JSON.stringify(Meteor.users.find().fetch(),undefined,2))
  //    const users= Meteor.users.find().fetch();
  //    console.log(users)
  //     for (let key in users) {
  //    console.log("user in Db ",users[key])
  //    Meteor._debug()
  //    debugger;
  //       if (!users[key].isAdmin) {

  //           for (let index in users[key].dates)
  //            {

  //             console.log("Zones",users[key].dates[index].zone, values.ZoneNumber)

  //           if (users[key].dates[index].zone == values.ZoneNumber) {

  //             var TimeStart = moment( values.FromTime)
  //             var TimeEnd = moment( values.ToTime)
  //             console.log("TimeStart", TimeStart)
  //             console.log("TimeEnd", TimeEnd)
  //             console.log("user.dates[key].OldFrom", moment( users[key].dates[index].OldFrom))
  //             console.log("user.dates[key].oldTo",  moment(users[key].dates[index].oldTo))

  //             if (  moment( users[key].dates[index].OldFrom).isBetween(TimeStart, TimeEnd) || moment( users[key].dates[index].oldTo).isBetween(TimeStart, TimeEnd)) {
  //               console.log("has to anothr user ")

  //             }




  //           }
  //         }
  //       }
  //     }
  //   }
  // })
// });

